/*! \file  testMZ02.c
 *
 *  \brief Blink the LED with FRCPLL
 *
 *
 *  \author jjmcd
 *  \date January 9, 2016, 3:19 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>

/*! main - */

/*!
 *
 */
int main(void)
{
  long i;
  int j;
    
  TRISFbits.TRISF2 = 0;     /* LED pin output */
  
  LATFbits.LATF2 = 1;   /* LED on */
  
  /* Blink the LED 10 times
   * 
   * LATFbits.LATF2 ^= 1 toggles the LED state so doing that
   * 20 times makes LED blink 10 times.
   * 
   * Clock should be 200 MHz
   * 
   * Loop count of 5,000,000 makes for a about a half second blink
   */
  while(1)
    {
      for (j = 0; j < 20; j++)
        {
          for (i = 0; i < 5000000; i++)
            ;
          LATFbits.LATF2 ^= 1;
        }
    }

  return 0;
}
