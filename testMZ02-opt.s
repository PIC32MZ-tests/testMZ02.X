!int main(void)
!{
0x9D00012C: LUI V1, 76
0x9D000174: J 0x9D000144
0x9D000178: ADDIU V0, V1, 19264
!  long i;
!  int j;
!    
!  TRISFbits.TRISF2 = 0;     /* LED pin output */
0x9D000110: LUI V1, -16506
0x9D000114: LHU A0, 1296(V1)
0x9D00011C: INS A0, ZERO, 2, 1
0x9D000120: SH A0, 1296(V1)
!  
!  LATFbits.LATF2 = 1;   /* LED on */
0x9D000118: LUI V0, -16506
0x9D000124: LHU A1, 1328(V0)
0x9D000130: ADDIU A2, ZERO, 1
0x9D000134: INS A1, A2, 2, 1
0x9D000138: SH A1, 1328(V0)
!  
!  /* Blink the LED 10 times
!   * 
!   * LATFbits.LATF2 ^= 1 toggles the LED state so doing that
!   * 20 times makes LED blink 10 times.
!   * 
!   * Clock should be 200 MHz
!   * 
!   * Loop count of 5,000,000 makes for a about a half second blink
!   */
!  while(1)
!    {
!      for (j = 0; j < 20; j++)
0x9D000128: ADDU A0, ZERO, ZERO
0x9D000158: ADDIU A0, A0, 1
0x9D000168: SLTI A2, A0, 20
0x9D000170: MOVZ A0, ZERO, A2
!        {
!          for (i = 0; i < 5000000; i++)
0x9D000148: BNEL V0, ZERO, 0x9D000148
0x9D00014C: ADDIU V0, V0, -1
!            ;
!          LATFbits.LATF2 ^= 1;
0x9D00013C: J 0x9D000174
0x9D000140: ADDU A1, V0, ZERO
0x9D000144: ADDIU V0, V0, -1
0x9D000150: LW A2, 1328(A1)
0x9D000154: LHU V0, 1328(A1)
0x9D00015C: EXT A2, A2, 2, 1
0x9D000160: XORI A2, A2, 1
0x9D000164: INS V0, A2, 2, 1
0x9D00016C: SH V0, 1328(A1)
0x9D000170: MOVZ A0, ZERO, A2
0x9D000174: J 0x9D000144
0x9D000178: ADDIU V0, V1, 19264
